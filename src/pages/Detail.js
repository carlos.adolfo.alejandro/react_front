import React, {Component} from 'react'
import PropTypes from 'prop-types'
import ButtonBackTohome from '../components/ButtonBackToHome'

const API_KEY='e4f7a44e'

export class Detail extends Component{
    static propTypes ={
        match: PropTypes.shape({
            params:PropTypes.object,
            isExact: PropTypes.bool,
            path: PropTypes.string,
            url: PropTypes.string
        })
    }

    state = {movie : {}}

    _fetchMovie({id}){
        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
            .then(response=> response.json())
            .then(movie =>{
                console.log({movie})
                this.setState({movie})
            })
    }
    _goBack(){
        window.history.back()
    }

    componentDidMount(){
        console.log(this.props)
        const {movieId} = this.props.match.params
        this._fetchMovie({id: movieId})
    }

    render(){
        console.log('renderDetail')
        const {Title, Poster, Actors, Metascore, Plot} = this.state.movie
        return(
            <div>
                <ButtonBackTohome />
                <h1>{Title}</h1>
                <img src={Poster} />
                <h3>{Actors}</h3>
                <span>{Metascore}</span>
                <p>{Plot}</p>
            </div>
        )
    }
}