import React from 'react'
import {Link} from 'react-router-dom'

const ButtonBackTohome = ()=>(
        <Link
            className='button is-info'
            to='/'>
            Volver a la portada        
        </Link>
)
 
export default ButtonBackTohome

