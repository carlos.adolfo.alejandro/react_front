import React, {Component} from 'react';

const API_KEY='e4f7a44e'

export class SearchForm extends Component{
    state={
        inputMovie: ''
    }

    _handleChange =(e)=> {
        console.log('handleChange')
        this.setState({inputMovie: e.target.value })
    }

    _handleSubmit =(e)=>{
        console.log('handleSubmit')
        e.preventDefault()
        const {inputMovie} = this.state
        fetch(`http://ec2-18-218-123-170.us-east-2.compute.amazonaws.com:8080/echo/asdasd121`)
            .then(response=> response.json())
            .then(results =>{
                console.log(results)
                //Ponemos valores por defecto
                const { Search = [], totalResults= "0"} = results
                console.log({Search, totalResults})
                this.props.onResults(Search)
            })
    }

    

    render(){
        console.log('renderSearchForm')
        return(
            <form onSubmit={this._handleSubmit}>
                <div className="field hass-addons">
                    <div className="control">
                        <input 
                            className="input" 
                            onChange={this._handleChange}
                            type="text" 
                            placeholder="Movie to search..." />
                    </div>
                    <div className="control">
                        <button className="button is-info">
                            Search
                        </button>
                    </div>
                </div>
            </form>
        )
    }
}