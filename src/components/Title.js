import React from 'react'

export const Title = ({children}) => (
    <h1 className="title">{children}</h1>
)

//export default lo podemos exportar con cualquier nombre
// export nombrado obligamos a usar el mismo nombre

