//importamos el metodo create Store desde redux
import { createStore } from 'redux'

//creamos nuestro reducer
function todos(state = [], action) {
    switch (action.type) {
        case 'ADD_TODO':
            return state.concat([action.text])
        default:
            return state
    }
}

//creamos la store pasandole el reducer
const store = createStore(todos)

//exportamos la store
export default store